#include <errno.h>
#include <poll.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/inotify.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>

static void handle_events(int fd, int wd, const char* fileName, unsigned int mask, const char* cmd)
{
   /* Some systems cannot read integer variables if they are not
      properly aligned. On other systems, incorrect alignment may
      decrease performance. Hence, the buffer used for reading from
      the inotify file descriptor should have the same alignment as
      struct inotify_event. */

   char buf[4096]
       __attribute__ ((aligned(__alignof__(struct inotify_event))));
   const struct inotify_event *event;
   ssize_t len;
   char *ptr;

   /* Loop while events can be read from inotify file descriptor. */

   for (;;) {

       /* Read some events. */

       len = read(fd, buf, sizeof buf);
       if (len == -1 && errno != EAGAIN) {
           perror("read");
           exit(EXIT_FAILURE);
       }

       /* If the nonblocking read() found no events to read, then
          it returns -1 with errno set to EAGAIN. In that case,
          we exit the loop. */

       if (len <= 0)
           break;

       /* Loop over all events in the buffer */

       for (ptr = buf; ptr < buf + len;
               ptr += sizeof(struct inotify_event) + event->len) {

           event = (const struct inotify_event *) ptr;

           if ((event->mask & mask) &&
               !(event->mask & IN_ISDIR) &&
               event->len && !strcmp(event->name, fileName))
           {
               printf("%s triggered\n", fileName);
               system(cmd);
           }
       }
   }
}

int main(int argc, char* argv[])
{
   int fd;
   int poll_num;
   nfds_t nfds;
   struct pollfd fds[2] = {{0}, {0}};

   if (argc != 5) {
       printf("Usage: %s WATCH_PATH FILE_NAME EVANT_MASK_VALUE CMD\n\tEVANT_MASK_VALUE:https://sites.uclouvain.be/SystInfo/usr/include/linux/inotify.h.html\n", argv[0]);
       exit(EXIT_FAILURE);
   }

   const char* dirPath = argv[1];
   const char* fileName = argv[2];
   unsigned int mask = 0;
   if (!sscanf(argv[3], "%d", &mask)) {
       perror("Wrong mask value!");
       exit(EXIT_FAILURE);
   }
   const char* cmd = argv[4];

   /* Create the file descriptor for accessing the inotify API */

   fd = inotify_init1(IN_NONBLOCK);
   if (fd == -1) {
       perror("inotify_init1");
       exit(EXIT_FAILURE);
   }


   /* Mark directories for events
      - file was opened
      - file was closed */
   int wd;
   wd = inotify_add_watch(fd, dirPath, mask);
   if (wd == -1) {
     fprintf(stderr, "Cannot watch '%s': %s\n",
             dirPath, strerror(errno));
     exit(EXIT_FAILURE);
   }


   /* Prepare for polling */

   nfds = 1;

   /* Inotify input */

   fds[0].fd = fd;
   fds[0].events = POLLIN;

   /* Wait for events and/or terminal input */

   printf("Listening for events.\n");
   while (1) {
       poll_num = poll(fds, nfds, -1);
       if (poll_num == -1) {
           if (errno == EINTR)
               continue;
           perror("poll");
           exit(EXIT_FAILURE);
       }

       if (poll_num > 0) {

           if (fds[0].revents & POLLIN) {

               /* Inotify events are available */

               handle_events(fd, wd, fileName, mask, cmd);
           }
       }
   }

   printf("Listening for events stopped.\n");

   /* Close inotify file descriptor */

   close(fd);

   exit(EXIT_SUCCESS);
}